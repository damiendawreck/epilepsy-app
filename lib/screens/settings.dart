import 'package:flutter/material.dart';

class SettingsScreen extends StatefulWidget {
  SettingsScreen() : super();

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      ListTile(title: Text("Alert Time"), subtitle: Text("After how long do we send an alert"),),
      ListTile(title: Text("Alert Volume"), subtitle: Text("What volume should the alert be"),),
    ],);
  }
}