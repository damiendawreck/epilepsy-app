import 'package:flutter/material.dart';

class StatisticsScreen extends StatefulWidget {
  StatisticsScreen() : super();

  @override
  _StatisticsScreenState createState() => _StatisticsScreenState();
}

class EpilepsyAlert {
  var time;
  IconData iconUsed = Icons.favorite;
  var text;

  EpilepsyAlert(var _time, IconData _iconUsed, var _text)
  {
    this.time = _time;
    this.iconUsed = _iconUsed;
    this.text = _text;
  }
}

class _StatisticsScreenState extends State<StatisticsScreen> {
  List<EpilepsyAlert> alerts = [
    new EpilepsyAlert("09:35", Icons.camera_alt, " Child may have woken up"),
    new EpilepsyAlert("08:45", Icons.warning, " Child experienced a seizure!"),
    new EpilepsyAlert("09:39", Icons.camera_alt, " Child may have woken up"),
    new EpilepsyAlert("09:36", Icons.camera_alt, " Crying sounds"),
    new EpilepsyAlert("07:10", Icons.person, " Unusual movements"),
    new EpilepsyAlert("04:25", Icons.favorite, " [045] Night's lowest BPM so far"),
    new EpilepsyAlert("04:04", Icons.favorite, " [086] High BPM. Likely reason: Nightmare")
  ];

  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      Container(
      margin: EdgeInsets.only(top:10),
        padding: EdgeInsets.all(16.0),
        width: double.infinity,
        color: Colors.black54,
        height: 390,
        child: ListView(children: [
          Column(children: [ for (var alert in alerts) RichText(
            text: TextSpan(
              children: [
                TextSpan(style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
                  text: alert.time,
                ),
                WidgetSpan(
                  child: Icon(alert.iconUsed, size: 20, color: Colors.lightBlue,),
                ),
                TextSpan(style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
                  text: alert.text,
                ),
              ],
            ),
          ) ]),
          Container(padding: EdgeInsets.only(top: 16.0), child: Text("More History...", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.lightBlueAccent)),),
        ],)),
    ],);
  }
}