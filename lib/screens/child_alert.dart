import 'package:flutter/material.dart';

class ChildAlert extends StatefulWidget {
  final int bpm;

  const ChildAlert({ Key? key, required this.bpm}) : super(key: key);

  @override
  _ChildAlertState createState() => _ChildAlertState();
}

class _ChildAlertState extends State<ChildAlert> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [Color(0xffFC5859), Color(0xffD4395B)]),),
      child: Center(
        child: Container(
          width: 200,
          height: 200,
          child: Column(
          children: [
            Text("ALERT", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 35),),
            Icon(Icons.favorite, color: Colors.white, size: 100,),
            Text(widget.bpm.toString() + " BPM", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 35),),
          ],
      ),
        ),),
    );
  }
}