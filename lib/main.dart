import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:app/screens/child_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

import 'screens/settings.dart';
import 'screens/statistics.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.cyan,
      ),
      home: MyHomePage(title: 'My Child', screen: new MyChildScreen(updateParent: () => {}),),
    );
  }
}

class MyChildScreen extends StatefulWidget {
  final Function updateParent;
  MyChildScreen({required this.updateParent}) : super();

  @override
  _MyChildScreenState createState() => _MyChildScreenState();
}

class _MyChildScreenState extends State<MyChildScreen> {
  bool alerting = true;
  ApiData status = new ApiData(bpm: 80, lyingPosition: "Upright", stable: true);

  Timer startTimeout([int? milliseconds]) {
    return Timer.periodic(new Duration(seconds: 5), (timer) {
      getData();
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    startTimeout();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  void getData() async
  {
    // Response data = await http.get(Uri.parse('https://832f-145-93-91-244.ngrok.io/children'));
    // var raw = jsonDecode(data.body);
    if(mounted)
    {
      var newStat = new ApiData(bpm: new Random().nextInt(120), lyingPosition: "Upright", stable: true);
      // var newStat = new ApiData(bpm: raw['bpm'], lyingPosition: raw['lyingPosition'], stable: raw['stable']);
      setState(() {
        status = newStat;
      });
      widget.updateParent(status);
    }
  }

  @override
  Widget build(BuildContext context) {
    // Widget alert = alerting ? Container(child: Column(children: [
    //   Container(child: Center(child: Text("ALERT", style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold, color: Colors.red)),),),
    //   Container(child: Center(child: Text("Your child may be having an attack!", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red)),),),
    // ],),) : Container();

    return Center(
      child: Container(
        height: 300,
        width: 300,
        child: Column(
          children: [
          Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  color: Color(0xff85FCA3).withOpacity(0.5),
                  spreadRadius: 60,
                  blurRadius: 60,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
              // gradient: LinearGradient(begin: Alignment.topLeft, end: Alignment.bottomRight, colors: [Colors.white, Color(0xff35C8A8)]),
            ),
            child: Container(
              constraints: BoxConstraints.expand(
                height: 200,
                width: 200
              ),
              decoration: new BoxDecoration(shape: BoxShape.circle,
                gradient: status.bpm > 99 ?
                LinearGradient(begin: Alignment.topLeft, end: Alignment.bottomRight, colors: [Color(0xffFC5859), Color(0xffD4395B)])
                :
                LinearGradient(begin: Alignment.topLeft, end: Alignment.bottomRight, colors: [Color(0xff85FCA3), Color(0xff35C8A8)]),
                border: Border.all(width: 3.0, color: Color(0xff46AD7E),)),
              child: Center(child: Container(child: Column(mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,children: [
                Center(
                  child: Container(
                    height: 150,
                    width: 130,
                    child: Row(
                      children: [
                        Icon(Icons.favorite, color: Colors.white, size: 50,),
                        Container(width: 80, child: Text(status.bpm.toString() + " BPM", textAlign: TextAlign.left, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 30),))
                      ],
                    ),
                  ),
                ),
              ],)))
              ),
          ),
          SizedBox(height: 20),
          Text("Monitoring Active", textAlign: TextAlign.center, style: TextStyle(color: Color(0xff35C8A8), fontWeight: FontWeight.bold, fontSize: 15)),
        ],
        ),
      ),
    );
  }

  // @override
  // Widget build(BuildContext context) {
  //   Widget alertTitle = ringtoneEnabled ? Text("ALERT", style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold, color: Colors.red)) : Container();
  //   Widget alert = ringtoneEnabled ? Container(child: Column(children: [
  //     Text("ALERT", style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold, color: Colors.red)),
  //     Text("Your child may be having an attack!", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red))
  //   ],),) : Container();
  //
  //   return ListView(children: [
  //     SizedBox(height: 20,),
  //     Container(child: Center(child: MaterialButton(child: Text("80 BPM", style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold)), onPressed: () {
  //       setState(() {
  //         ringtoneEnabled = !ringtoneEnabled;
  //       });
  //
  //     },),),),
  //     alert,
  //   ],);
  // }
}

class ApiData
{
  final int bpm;
  final String lyingPosition;
  final bool stable;

  ApiData({required this.bpm, required this.lyingPosition, required this.stable});
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title, required this.screen, this.showAppbar = false}) : super(key: key);

  final String title;
  final Widget screen;
  final bool showAppbar;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late bool showAppbar;
  bool alert = false;
  ApiData status = new ApiData(bpm: 80, lyingPosition: "Upright", stable: true);
  int bpm = 80;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    showAppbar = widget.showAppbar;

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: alert ? null : !showAppbar ? null : AppBar(
        title: Text(widget.title, style: TextStyle(color: Colors.white),),
        backgroundColor: Color(0xff33CAA1),
      ),
      bottomNavigationBar: BottomAppBar(shape: const CircularNotchedRectangle(), color: Color(0xff74EEA0),
      child: IconTheme(
        data: IconThemeData(color: Theme.of(context).colorScheme.onPrimary),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconButton(
              tooltip: 'Statistics',
              icon: const Icon(Icons.query_stats),
              color: Colors.white,
              onPressed: () => {
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
                  return MyHomePage(title: 'Statistics', screen: new StatisticsScreen(), showAppbar: true,);
                })),
              },
            ),
            IconButton(
              tooltip: 'Settings',
              icon: const Icon(Icons.settings),
              color: Colors.white,
              onPressed: () => {
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
                  return MyHomePage(title: 'Settings', screen: new SettingsScreen(), showAppbar: true,);
                })),
              },
            ),
          ],
        ),
      )),
      floatingActionButton: FloatingActionButton(onPressed: () => {
        if(showAppbar)
        {
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
            return MyHomePage(title: 'My Child', screen: new MyChildScreen(updateParent: (a) => { setState(() => { status: a })}));
          })),
        }
        else
        {
          setState(() => {alert = !alert})
        }
      },
      child: Icon(Icons.emoji_people), backgroundColor: Color(0xff33CAA1), foregroundColor: Colors.white, ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: AnimatedSwitcher(
        duration: Duration(milliseconds: 500),
        child: alert ? ChildAlert(bpm: status.bpm,) : Container(child: widget.screen),
      ),
    );
  }
}
